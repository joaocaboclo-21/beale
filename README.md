João Marcelo Sales Caboclo Ribeiro GRR-20221227

1. "beale.c"
    Este arquivo contem a função main, o arquivo usa a função "getopt" para analisar os argumentos inseriods. Caso os arguemntos estejam erradas ou organizados
de forma diferente do que foi pedido uma menagem de erro sera mostrada. Caso contrario, uma função que trata o que foi pedido sera chamada de acordo com os 
argumentos inseridos.

2. "lib_armazenamnto.c"
    Este arquivo possui duas finalidades diferentes, mas que estao relacionadas com a forma que usei para armazenar os dados.

    2.1. Estrutura e dados e manipulação das estruturas.
        Como estrutura de dados usei "struct armazenamento" que contem inteiros que auxiliam na manipulação da estrutura e também um vetor de ponteiros para
        ponteiros. Esse vetor abriga a "struct no_chaves" essa estrutura contem uma char que representa uma chave, um inteiro que representa o numero de numeros
        que representam a respectiva chave e um ponteiro que aponta para outra estrutura a "struct no_numeros". Essa estrutura contem um inteiro que representa
        o numero que ela representa e um ponteiro que aponta para outra estutura "struct no_numeros" assim  configurando uma lista ligada.
        Para a manipulação da estrutura foi necessario uma função para adicionar uma chave (consequentemente uma função para aumentar o tamanho do vetor de
        ponteiros para ponteiros), uma função para adicionar um novo numero na lista ligada, uma função para obter uma chave de acordo com um dos seus numeros
        representantes e por ultimo uma função para obter um numero aleatorio representante de uma chave especifica.

    2.2. Leitura de LivroCifra e ArquivoDeChaves.
        Foram usadas duas funções para a leitura de arquivos. A função "le_cifra" que analisa a primeira letra de uma palavra, cria uma "strcut no_chaves" e a
        adiciona ao vetor de ponteiros de ponteiros, alem disso obtem a posição da palavra e adiciona o valor na "struct no_numeros" de sua respectiva chave.
        A função "le_chaves" analisa o arquivo de chaves string por string. Se o primeiro indice da string nao for um digito e o segundo for um ":", o primeiro
        sera considerado uma chave e sera criado uma "struct no_chaves" para este. Caso contrario sera criado uma "struct no_numeros" que sera adicionado ao
        "no_chaves" até ser lido uma nova "char:".

3. "lib_codifica"
    Este arquivo possui tres funções. "trata_codifica", que foi a chamada pelo arquivo "beale.c" que tem como finalidade abrir e conferir os arquivos necessarios
    e ao final fechar os mesmos arquivos e liberar o armazenamento, além disso essa função chama as duas outras do arquivo e a "le_cifra" já explicada anteriormente.
    A função "codidica", tem como finalidade analisar o texto original e usar a funcao "procura_numero" para substituir a respectiva letra do texto por um dos seus
    representantes numericos ou o espaço por -1. A função "printa_chaves", imprime em um arquivo de forma organizada, o armazenamento obtido pela a leitura
    do LivroDeCifras.

4. "lib_decodifica"
    Este arquivo possui tres funções. "trata_decodifica", que foi a chamada pelo arquivo "beale.c" que tem como finalidade abrir e conferir os arquivos necessarios
    e ao final fechar os mesmos arquivos e liberar o armazenamento, além disso essa função separa se a decodificação vai ser com base do LibroDeCifras, portanto
    chamando a função "le_cifra", ou do ArquivoDeChaves chamando "le_chaves". A funcão "decodificada" usa a função "procura_chave" para obter uma chave de acordo com
    seu representante de posição numerico. Além disso exite a função "casos_especiais" que confere se o caracter lido é um espaço ou pontuaçao, caso seja uma pontuação
    registra que o proximo caracter adicionado deve estar em letra maiuscula caso seje uma letra.