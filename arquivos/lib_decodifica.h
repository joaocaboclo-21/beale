#ifndef LIB_DECODIFICA_H
#define LIB_DECODIFICA_h

#include <stdio.h>
#include <stdlib.h>
#include "lib_armazenamento.h"


void trata_decodifica(char *M_Codificada, char *Chave_ou_Cifra, char *M_Decodificada , int eh_chave);

void decodifica(FILE *MensagemCodificada, FILE* MensagemDecodificada, struct armazenamento *arm);

void printa_chaves(FILE *ArquivoDeChaves, struct armazenamento *arm);

#endif