#include "lib_armazenamento.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

/*Tamanho da maior palavra existente (grego antigo)*/
#define MAXIMO_STRING 182

struct armazenamento *cria_armazenamento(){
    struct armazenamento *armazenamento = malloc(sizeof(struct armazenamento));
    armazenamento->numero_de_chaves = 0;
    armazenamento->tamanho_realloc = 0;
    return armazenamento;
}

struct no_chaves *cria_no_chaves(){
    struct no_chaves *no_chave = malloc(sizeof(struct no_chaves));
    no_chave->numero = NULL;
    no_chave->numero_de_codigos = 0;
    return no_chave;
}

struct no_numeros *cria_no_numeros(){
    struct no_numeros *no_numero = malloc(sizeof(struct no_numeros));
    no_numero->prox = NULL;
    return no_numero;
}

/*Inicia armazenamnto e ajusta o tamanho de realloc para o proximo aumento da struct*/
struct no_chaves **inicia_armazenamento(struct armazenamento *arm){
    struct no_chaves **no_chave = malloc(sizeof(struct no_chaves));
    arm->tamanho_realloc++;
    return no_chave;
}

void aumenta_armazenamento(struct armazenamento *arm){
    arm->numero_de_chaves++;
    arm->tamanho_realloc++;
    arm->arm_chaves = realloc(arm->arm_chaves, sizeof(struct no_chaves) * arm->tamanho_realloc);
}

/*Funcao para auxiliar no debug*/
void printa_armazenamento(struct armazenamento *arm){
    int i;
    for (i = 0; i <= arm->numero_de_chaves; i++){
        printf("%c:", arm->arm_chaves[i]->chave);
        struct no_numeros *no_numero = arm->arm_chaves[i]->numero;
        
        while (no_numero != NULL){
            printf("%d ", no_numero->numero);

            no_numero = no_numero->prox;
        }

        printf("\n");

    }
}

/*Para facilitar a utilização das funcoes*/
void atribui_numero_chave(char chave, int numero, struct armazenamento *arm){
    adiciona_chave(chave, arm);
    adiciona_numero(numero, chave, arm);
}


void adiciona_chave(char chave, struct armazenamento *arm){

    if (arm->tamanho_realloc == 0){
        arm->arm_chaves = inicia_armazenamento(arm);

        struct no_chaves *no_chave = cria_no_chaves();
        no_chave->chave = chave;
        arm->arm_chaves[arm->numero_de_chaves] = no_chave;
        return;
    }

    int i;
    /*Caso a chave ja tenha sido inserida anteriormente*/
    for (i = 0; i <= arm->numero_de_chaves; i++){

        if (arm->arm_chaves[i]->chave == chave)
            return;
    }

    aumenta_armazenamento(arm);
    struct no_chaves *no_chave = cria_no_chaves();
    no_chave->chave = chave;
    arm->arm_chaves[arm->numero_de_chaves] = no_chave;
    return;
}

void adiciona_numero (int numero, char chave, struct armazenamento *arm){
    struct no_numeros *no_numero = cria_no_numeros();
    no_numero->numero = numero;
    struct no_chaves *no_chave;
    int i;

    for (i = 0; i <= arm->numero_de_chaves; i++){
        if (arm->arm_chaves[i]->chave == chave){
            no_chave = arm->arm_chaves[i];
            arm->arm_chaves[i]->numero_de_codigos++;
            break;
        }
    }

    if (no_chave != NULL){
        no_numero->prox = no_chave->numero;
        no_chave->numero = no_numero;
        return; 
    }

    no_chave->numero = no_numero;
}

struct no_chaves *procura_chave(int numero, struct armazenamento *arm){
    int i, j;
    struct no_numeros *no_numero;

    for (i = 0; i <= arm->numero_de_chaves; i++){

        no_numero = arm->arm_chaves[i]->numero;
        for (j = 0; j < arm->arm_chaves[i]->numero_de_codigos; j++){

            if (no_numero->numero == numero){
                return arm->arm_chaves[i];
            }

            no_numero = no_numero->prox;
        }
    }

    return NULL;
}

struct no_numeros *procura_numero(char chave, struct armazenamento *arm){
    int i, j; 
    int posicao = 0;
    struct no_numeros *aux;

    for (i = 0; i <= arm->numero_de_chaves; i++){

        if (arm->arm_chaves[i]->chave == chave){
            
            posicao = rand() % arm->arm_chaves[i]->numero_de_codigos;
   
            aux = arm->arm_chaves[i]->numero;

            for (j = 0; j < posicao; j++){
                aux = aux->prox;
            }

            return aux;

        }
    }
    return NULL;
}

void le_cifra(FILE* LivroCifra, struct armazenamento *arm){

    char string[MAXIMO_STRING];
    char primeira_letra;

    int posicao_palavra = 0;

    fscanf(LivroCifra, "%s", string);
    while (!feof(LivroCifra)){

        primeira_letra = tolower(string[0]);
        atribui_numero_chave(primeira_letra, posicao_palavra, arm);
        posicao_palavra++;

        fscanf(LivroCifra, "%s", string);
    }
}

void le_chaves(FILE *ArquivoDeChaves, struct armazenamento *arm){
    char string[MAXIMO_STRING];
    int numero;
    char chave;

    fscanf(ArquivoDeChaves, "%s", string);
    while (!feof(ArquivoDeChaves)){

        if(string[1] == ':' || !isdigit(string[0])){
            adiciona_chave(string[0], arm);
            chave = string[0];
        }
        else{
            numero = atoi(string);
            adiciona_numero(numero, chave, arm);
        }
        fscanf(ArquivoDeChaves, "%s", string);
    }
}

void libera_armazenamneto(struct armazenamento *arm){
    int i;

    for (i = 0; i <= arm->numero_de_chaves; i++){

        if (arm->arm_chaves[i]->numero != NULL){
            struct no_numeros *atual = arm->arm_chaves[i]->numero, *proxNo;

            while (atual != NULL){
                proxNo = atual->prox;
                free(atual);
                atual = proxNo;
            }
        }

        free(arm->arm_chaves[i]);
    }
    free(arm->arm_chaves);
    free(arm);
}