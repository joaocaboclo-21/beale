#include "lib_armazenamento.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

#define MAXIMO_STRING 182

int casos_especiais(char *codigo, int *anterior, FILE *MensagemDecodificada){

    if (strcmp(codigo, "-1") == 0){

        fprintf(MensagemDecodificada, "%c", ' ');
        return 1;
    }

    else if(!isdigit(codigo[0])){

        fprintf(MensagemDecodificada, "%s", codigo);

        if (strcmp(codigo, ".") == 0 || strcmp(codigo, "?") == 0 || strcmp(codigo, "!") == 0 || strcmp(codigo, ":") == 0){
            *anterior = 1;
        }

        return 1;

    }

    return 0;

}

void decodifica(FILE *MensagemCodificada, FILE* MensagemDecodificada, struct armazenamento *arm){

    struct no_chaves *no_chave;
    char codigo[MAXIMO_STRING];
    int numero, anterior = 1;

    fscanf(MensagemCodificada, "%s", codigo);

    while(!feof(MensagemCodificada)){

        if (!casos_especiais(codigo, &anterior, MensagemDecodificada)){
            
            numero = atoi(codigo);
            no_chave = procura_chave(numero, arm);

            if(no_chave == NULL){
                fprintf(MensagemDecodificada, "%s", codigo);
            }

            else{
                
                if (anterior){
                   fprintf(MensagemDecodificada, "%c", toupper(no_chave->chave)); 
                   anterior = 0;
                }
                else{
                    fprintf(MensagemDecodificada, "%c", tolower(no_chave->chave));
                }
            }
        }

        fscanf(MensagemCodificada, "%s", codigo);
    }

}


void trata_decodifica(char *M_Codificada, char *Chave_ou_Cifra, char *M_Decodificada , int eh_chave){

    struct armazenamento *arm = cria_armazenamento();
    FILE *MensagemCodificada = fopen(M_Codificada, "r");
    FILE *MensagemDecodificada = fopen(M_Decodificada, "w");

    if(!MensagemCodificada || !MensagemDecodificada){
        perror ("Erro ao abrir/criar arquivo");
        exit(1);
    }

    if (eh_chave){
        FILE *ArquivoDeChaves = fopen(Chave_ou_Cifra, "r");

        if(!ArquivoDeChaves){
            perror ("Erro ao abrir/criar arquivo");
            exit(1);
        }

        le_chaves(ArquivoDeChaves, arm);
        decodifica(MensagemCodificada, MensagemDecodificada, arm);

        fclose(ArquivoDeChaves);
    }

    else{
        FILE *LivroCifra = fopen(Chave_ou_Cifra, "r");

        if(!LivroCifra){
            perror ("Erro ao abrir/criar arquivo");
            exit(1);
        }

        le_cifra(LivroCifra, arm);
        decodifica(MensagemCodificada, MensagemDecodificada, arm);

        fclose(LivroCifra);
    }

    fclose(MensagemCodificada);
    fclose(MensagemDecodificada);
    libera_armazenamneto(arm);
}
