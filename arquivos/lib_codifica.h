#ifndef LIB_CODIFICA_H
#define LIB_CODIFICA_H

#include <stdio.h>
#include <stdlib.h>
#include "lib_armazenamento.h"

void trata_codifica(char *L_Cifra, char *M_Original, char *M_Codificada, char *Arq_Chaves);

void codifica(FILE* MensagemOriginal, FILE* MensagemCodificada, struct armazenamento *armazenamento);

int casos_especiais(char *codigo, int *anterior, FILE *MensagemDecodificada);

#endif