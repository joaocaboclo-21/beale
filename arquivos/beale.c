#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <getopt.h>
#include "lib_codifica.h"
#include "lib_decodifica.h"
#include <time.h>

int main(int argc, char *argv[]){

    srand((unsigned)time(NULL));

    int option;
    int flag_e = 0, flag_d = 0;
    char *endereco_i = NULL, *endereco_b = NULL, *endereco_c = NULL, *endereco_m = NULL, *endereco_o = NULL;

    while ((option = getopt(argc, argv, "ed i:b:m:c:o:")) != -1){
        switch (option){
            case 'e':
                flag_e = 1;
                break;
            case 'd':
                flag_d = 1;
                break;
            case 'i':
                endereco_i = optarg;
                break;
            case 'b':
                endereco_b = optarg;
                break;
            case 'c':
                endereco_c = optarg;
                break;
            case 'm':
                endereco_m = optarg;
                break;
            case 'o':
                endereco_o = optarg;
                break;

            default:
                perror("Os argumentos inseridos sao invalidos. Forma correta:\n");
                printf("./beale  -e  -b LivroCifra -m MensagemOriginal -o MensagemCodificada -c ArquivoDeChaves\n");
                printf("./beale  -d  -i MensagemCodificada  -c ArquivoDeChaves  -o MensagemDecodificada\n");
                printf("./beale -d -i MensagemCodificada -b LivroCifra -o MensagemDecodificada\n");
                exit(1);
       }
    }
    if (flag_e && !flag_d && !endereco_i && endereco_c && endereco_o && endereco_m && endereco_b){
        trata_codifica(endereco_b, endereco_m, endereco_o, endereco_c);
    } 

    else if ((endereco_c && flag_d && !endereco_b && !endereco_m && endereco_i && endereco_c && endereco_i)){
        trata_decodifica(endereco_i, endereco_c, endereco_o, 1);
    }

    else if (endereco_b && flag_d && !endereco_c && !endereco_m && endereco_i && endereco_b && endereco_o){
        trata_decodifica(endereco_i, endereco_b, endereco_o, 0);
    }
    else{
        printf("Os foram inseridos de forma incorreta. Forma correta:\n");
        printf("./beale  -e  -b LivroCifra -m MensagemOriginal -o MensagemCodificada -c ArquivoDeChaves\n");
        printf("./beale  -d  -i MensagemCodificada  -c ArquivoDeChaves  -o MensagemDecodificada\n");
        printf("./beale -d -i MensagemCodificada -b LivroCifra -o MensagemDecodificada\n");
    }
    return 0;
}
