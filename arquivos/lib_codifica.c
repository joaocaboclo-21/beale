#include "lib_armazenamento.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

void printa_chaves(FILE *ArquivoDeChaves, struct armazenamento *arm){

    int i, j;
    for (i = 0; i <= arm->numero_de_chaves; i++){
        
        fprintf(ArquivoDeChaves, "%c: ", arm->arm_chaves[i]->chave);

        struct no_numeros *no_numero = arm->arm_chaves[i]->numero;
        for(j = 0; j < arm->arm_chaves[i]->numero_de_codigos; j++){
            fprintf(ArquivoDeChaves, "%d ", no_numero->numero);
            no_numero = no_numero->prox;
        }

        fprintf(ArquivoDeChaves, "\n");
    }
}

void codifica(FILE* MensagemOriginal, FILE* MensagemCodificada, struct armazenamento *armazenamento){
    struct no_numeros *no_numero;
    char letra;
    
    fscanf(MensagemOriginal, "%c", &letra);

    while (!feof(MensagemOriginal)){

        if(letra == ' '){
            fprintf(MensagemCodificada, "%s ", "-1");
        }
        else{
            
            if (letra <= 'Z' && letra >= 'A'){
                letra = tolower(letra);
            }
            
            no_numero = procura_numero(letra, armazenamento);

            if (no_numero != NULL){
                fprintf(MensagemCodificada, "%d ", no_numero->numero);
            }
            else{
                fprintf(MensagemCodificada, "%c ", letra);
            }
        }

        fscanf(MensagemOriginal, "%c", &letra);
    }
}

void trata_codifica(char *L_Cifra, char *M_Original, char *M_Codificada, char *Arq_Chaves){

    FILE* LivroCifra = fopen(L_Cifra, "r");
    FILE* MensagemOriginal = fopen(M_Original, "r");
    FILE* MensagemCodificada = fopen(M_Codificada, "w");
    FILE* ArquivoDeChaves = fopen(Arq_Chaves, "w");

    if(!LivroCifra || !MensagemOriginal || !MensagemCodificada || !ArquivoDeChaves){
        perror ("Erro ao abrir/criar arquivo");
        exit(1);
    }
    else{
        struct armazenamento *armazenamento = cria_armazenamento();
        le_cifra(LivroCifra, armazenamento);
        codifica(MensagemOriginal, MensagemCodificada, armazenamento);

        printa_chaves(ArquivoDeChaves, armazenamento);
        libera_armazenamneto(armazenamento);

        fclose(LivroCifra);
        fclose(MensagemOriginal);
        fclose(MensagemCodificada);
        fclose(ArquivoDeChaves);

        printf("Sucesso na codificação\n");
    }
}
