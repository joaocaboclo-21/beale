#ifndef LIB_ARMAZENAMENTO_H
#define LIB_ARMAZENAMENTO_H

#include <stdlib.h>
#include <stdio.h>


struct no_numeros{
    int numero;
    struct no_numeros *prox;
};

struct no_chaves{
    char chave;
    int numero_de_codigos;
    struct no_numeros *numero;
};

struct armazenamento{
    struct no_chaves **arm_chaves;
    int numero_de_chaves;
    int tamanho_realloc;
};

struct armazenamento *cria_armazenamento();

/*Funcao para aumentar o tamanho do vetor armazenamento toda vez que uma nova chave por ser inserida*/
void aumenta_armazenamento(struct armazenamento *arm);

/*Retorna um no_numeros aleatorio entre aqueles que representam a respectiva chave */
struct no_numeros *procura_numero(char chave, struct armazenamento *arm);

/*Aumenta o tamanho do vetor armazenamento e adiciona uma struct no_chaves no novo espaco do armazenamento*/
void adiciona_chave(char chave, struct armazenamento *arm);

/*Retorna o no_chaves em que o numero esta representado*/
struct no_chaves *procura_chave(int numero, struct armazenamento *arm);

/*Adiciona um no_numero na primeira posicao da lista ligada seguida do no_chave*/
void adiciona_numero(int numero, char chave, struct armazenamento *arm);

void libera_armazenamneto(struct armazenamento *arm);

/*Le sring por string e monta struct armazenamento de acordo com a primeira letra e posiçao da palavra*/
void le_cifra(FILE* LivroCifra, struct armazenamento *arm);

/*Le arquivo de chaves e monta struct armazenamento de acordo com a organizaçao*/
void le_chaves(FILE *ArquivoChaves, struct armazenamento *arm);

#endif